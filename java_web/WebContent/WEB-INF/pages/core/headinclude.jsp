<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<% 
	String path = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<title>管理系统</title>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- viewport的<meta>标签，这个标签可以修改在大部分的移动设备上面的显示，为了确保适当的绘制和触屏缩放。-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--Bootstrap CSS -->
<%-- <link href="${ctx}/ui/widgets/bootstrap3/css/bootstrap.min.css" rel="stylesheet">
<link href="${ctx}/ui/widgets/bootstrap3/font-awesome/css/font-awesome.css" rel="stylesheet">
<!-- Chosen -->
<script src="${ctx}/ui/widgets/bootstrap3/js/plugins/chosen/chosen.jquery.js"></script> --%>
<!--  Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet" href="${ctx}/ui/lib/bootstrap-3.3.5-dist/css/bootstrap.min.css">
<link rel="stylesheet" href="${ctx}/ui/lib/bootstrap-3.3.5-dist/css/bootstrap-theme.min.css">
<script src="${ctx}/ui/lib/jquery-1.11.3/jquery.min.js"></script>
<script src="${ctx}/ui/lib/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
<script type="text/javascript">
	if(window.G_APP === undefined) {
		window.G_APP = {};
	}
	G_APP.WebContext = "<%=path%>";
</script>
