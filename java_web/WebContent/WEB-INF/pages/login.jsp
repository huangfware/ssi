<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="core/headinclude.jsp"%>
<title>Insert title here</title>
<script type="text/javascript">
	$(function() {
		var height = $(window).height();  
		$('.container').css('margin-top', height / 3);
	});
</script>
<style type="text/css">
	body {
		background-color: gray;
	}
</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<form class="form-horizontal" action="login.do" method="post">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-4 control-label">登录名</label>
						<div class="col-sm-8">
							<input type="text" name="username" class="form-control" id="inputEmail3"
								placeholder="登录名" />
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-4 control-label">密&nbsp;&nbsp;&nbsp;&nbsp;码</label>
						<div class="col-sm-8">
							<input type="password" name="password" class="form-control" id="inputPassword3"
								placeholder="密码" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<div class="checkbox">
								<label> <input type="checkbox" /> 记住我
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-10">
							<button type="submit" class="btn btn-default">登录</button>
						</div>
					</div>
				</form>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
</body>
</html>