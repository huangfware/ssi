<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="core/headinclude.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>测试页面</title>
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="http://echarts.baidu.com/build/dist/echarts.js"></script>
<script type="text/javascript">
	// 游戏启动次数， 客户端进入付费页面次数，客户端付费页面点击确认次数，客户端付费页面点击获取验证码次数，
	// 平台收到请求次数，平台用户鉴权通过次数，平台业务鉴权通过次数，平台其他鉴权通过次数，调用付费平台的付费用户次数
	// 平台付费成功次数 
	$(function() {
		// 路径配置
		require.config({
			paths : {
				echarts : 'http://echarts.baidu.com/build/dist'
			}
		});
		// 使用
		require([ 'echarts', 'echarts/chart/funnel' // 使用柱状图就加载bar模块，按需加载
		], function(ec) {
			// 基于准备好的dom，初始化echarts图表
			var myChart = ec.init(document.getElementById('main'));

			var option = {
				title : {
					text : '漏斗图',
					subtext : '纯属虚构'
				},
				tooltip : {
					trigger : 'item',
					formatter : "{a} <br/>{b}"
				},
				/* toolbox : {
					show : true,
					feature : {
						mark : {
							show : true
						},
						dataView : {
							show : true,
							readOnly : false
						},
						restore : {
							show : true
						},
						saveAsImage : {
							show : true
						}
					}
				}, */
				/* legend : {
					//data : [ '展现', '点击', '访问', '咨询', '订单' ]
					data:['游戏启动次数','客户端进入付费页面次数','客户端付费页面点击确认次数',
					      '客户端付费页面点击获取验证码次数','平台收到请求次数','平台用户鉴权通过次数',
					      '平台业务鉴权通过次数','平台其他鉴权通过次数','调用付费平台的付费用户次数',
					  	  '平台付费成功次数']
				}, */
				// calculable : true,
				series : [ {
					name : '漏斗图',
					type : 'funnel',
					width : '80%',
					legendHoverLink:false,
					clickable:false,
					itemStyle: {
			                normal: {
			                    label: {
			                       position: 'inside',
			                       labelLine:false,
								   textStyle: {
									   color:'black',
									   align:'center',
									   baseline:'top',
									   fontSize: 13
								   } 
			                    }
			                },
			                emphasis: {
			                    label: {
			                       position: 'inside',
			                       labelLine:true,
								   textStyle: {
									   color:'red',
									   align:'center',
									   baseline:'top',
									   fontFamily: 'Arial, Verdana, sans-serif',
									   fontSize: 14
								   } 
			                    }
			                }
			            },
					data : [ {
						value : 100,
						name : '游戏启动次数\n转化率54%',
						itemStyle : {
							normal: {
								color:'blue'								
							},
							emphasis: {
								color:'blue'
							}
						}
					}, {
						value : 90,
						name : '客户端进入付费页面次数\n转化率54%',
						itemStyle : {
							normal: {
								color:'green'								
							},
							emphasis: {
								color:'green'
							}
						}
					}, {
						value : 80,
						name : '客户端付费页面点击确认次数\n转化率54%'
					}, {
						value : 70,
						name : '客户端付费页面点击获取验证码次数\n转化率54%'
					}, {
						value : 60,
						name : '平台收到请求次数\n转化率54%'
					} , {
						value : 50,
						name : '平台用户鉴权通过次数\n转化率54%'
					} , {
						value : 40,
						name : '平台业务鉴权通过次数\n转化率54%'
					} , {
						value : 30,
						name : '平台其他鉴权通过次数\n转化率54%'
					} , {
						value : 20,
						name : '调用付费平台的付费用户次数\n转化率54%'
					} , {
						value : 10,
						name : '平台付费成功次数'
					}]
				} ]
			};
			// 为echarts对象加载数据 
			myChart.setOption(option);

		});
	})
</script>
<style type="text/css">
	
</style>
</head>
<body>
	<div style="height: 400px; width: 140px; position: absolute; top:75px; left: 85px; border-left: gray dotted 1px;
	border-bottom: gray dotted 1px;">
		<div style="position: absolute; top: 300px; left: -30px;
		background-color: #FFF;">转化率45%</div>
	</div>
	<div id="main" style="height: 550px; width: 600px;"></div>
</body>
</html>