package com.java.dao;

import com.java.bean.UserInfo;

public interface UserDAO {

    /**
     * 
     * 根据用户名查询用户密码
     * <功能详细描述>
     * @param username
     * @return
     */
    public UserInfo selectUserByName(String username);
    
}
