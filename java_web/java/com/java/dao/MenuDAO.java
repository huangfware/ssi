package com.java.dao;

import java.util.List;

import com.java.bean.MenuInfo;

public interface MenuDAO {

    public List<MenuInfo> selectMenusByUserId(String iid);
	
}
