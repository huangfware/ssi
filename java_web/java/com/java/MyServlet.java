package com.java;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 *  测试用的servlet，当中的计数功能
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  
 * @version  [版本号, 2016年2月15日]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class MyServlet extends HttpServlet{
  
    private long count;
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse rep) {
	System.out.println("次数：" + count++);
    }
}
