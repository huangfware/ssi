package com.java.util;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonUtil {
    
    Logger log = LoggerFactory.getLogger(JsonUtil.class);

    public static String objectToString(Object object) {
	
	return JSONObject.wrap(object).toString();
    }
}
