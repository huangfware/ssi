package com.java.util.interceptor;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * <一句话功能简述> <功能详细描述>
 * 
 * @author
 * @version [版本号, 2016年1月15日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class LoginCheckInterceptor extends BaseInterceptor {

    Logger log = LoggerFactory.getLogger(getClass());
    
    private List<String> uncheckUrls;

    private long count = 0;
    @Override
    public boolean before(HttpServletRequest req, HttpServletResponse rep, Object paramObject) {
//	String requestUrl = req.getRequestURI().replace(req.getContextPath(), "");
//	if (!uncheckUrls.contains(requestUrl)) {
//	    UserInfo user = (UserInfo) req.getSession().getAttribute(Constants.USER_INFO);
//	    if (null == user) {
//		try {
//		    rep.sendRedirect(req.getContextPath() + "/toLogin.do");
//		    return false;
//		} catch (IOException e) {
//		    log.error("erroe:", e);
//		}
//	    }
//	}
	return true;
    }

    @Override
    public void after(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse,
	    Object paramObject, ModelAndView paramModelAndView) {
	// TODO Auto-generated method stub
//	System.out.println("after");
    }

    @Override
    public void complete(HttpServletRequest paramHttpServletRequest, HttpServletResponse paramHttpServletResponse,
	    Object paramObject, Exception paramException) {
	// TODO Auto-generated method stub
	System.out.println("complete：" + count++);
	System.out.println(this.getClass().hashCode());
	System.out.println(this.getClass().getClassLoader());
    }

    public List<String> getUncheckUrls() {
	return uncheckUrls;
    }

    public void setUncheckUrls(List<String> uncheckUrls) {
	this.uncheckUrls = uncheckUrls;
    }

}
