package com.java.util.mvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.support.WebBindingInitializer;
import org.springframework.web.context.request.WebRequest;

public class MvcDataBinder
  implements WebBindingInitializer
{
  private List<String> dateFormats = null;

  public MvcDataBinder()
  {
    this.dateFormats = new ArrayList<String>();
    this.dateFormats.add("yyyy-MM-dd HH:mm:ss");
    this.dateFormats.add("yyyy-MM-dd HH:mm");
    this.dateFormats.add("yyyyMMdd-HHmmss");
    this.dateFormats.add("yyyy-MM-dd");
    this.dateFormats.add("MM-dd");
    this.dateFormats.add("HH:mm:ss");
    this.dateFormats.add("yyyy-MM");
    
  }

  public void initBinder(WebDataBinder binder, WebRequest request)
  {
    binder.registerCustomEditor(Date.class, 
      new MvcDateConverter((String[])this.dateFormats.toArray(new String[this.dateFormats.size()])));
  }

  public void setDateFormats(List<String> dateFormats) {
    if (dateFormats != null)
      this.dateFormats.addAll(dateFormats);
  }
}
