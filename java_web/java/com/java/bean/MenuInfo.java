package com.java.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 菜单信息
 * 
 * @author lj
 *
 */
public class MenuInfo implements Serializable{

    /**
     * 注释内容
     */
    private static final long serialVersionUID = 4916971579842159332L;
    /**
     * 
     * 菜单ID
     */
    private int iid;
    /**
     * 菜单显示名
     */
    private String name;
    /**
     * 菜单链接URL
     */
    private String linkUrl;
    /**
     * 菜单级别
     */
    private int menuLevel;
    /**
     * 父菜单ID
     */
    private int pid;
    /**
     * 排序
     */
    private int sequence_index;
    /**
     * 是否选中
     */
    private int isChecked;

    public List<MenuInfo> children = new ArrayList<MenuInfo>();

    public int getIid() {
        return iid;
    }

    public void setIid(int iid) {
        this.iid = iid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public int getMenuLevel() {
        return menuLevel;
    }

    public void setMenuLevel(int menuLevel) {
        this.menuLevel = menuLevel;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(int isChecked) {
        this.isChecked = isChecked;
    }

    public List<MenuInfo> getChildren() {
        return children;
    }

    public void setChildren(List<MenuInfo> children) {
        this.children = children;
    }

    public int getSequence_index() {
        return sequence_index;
    }

    public void setSequence_index(int sequence_index) {
        this.sequence_index = sequence_index;
    }

    
    
}
