package com.java.bean;

import java.io.Serializable;

public class UserInfo implements Serializable {

    private static final long serialVersionUID = -780576428509989037L;
    /**
     * 主键
     */
    private Integer iid;
    /**
     * 登陆名称
     */
    private String loginName;
    /**
     * 密码
     */
    private String pwd;
    /**
     * 真实姓名
     */
    private String name;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 性别 男 0 女 1
     */
    private Integer sex;
    /**
     * 是否有效
     */
    private Integer enable;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 电话
     */
    private String mobile;
    /**
     * email
     */
    private String email;
    /**
     * qq
     */
    private String qq;
    /**
     * 地址
     */
    private String address;

    public Integer getIid() {
	return iid;
    }

    public void setIid(Integer iid) {
	this.iid = iid;
    }

    public String getLoginName() {
	return loginName;
    }

    public void setLoginName(String loginName) {
	this.loginName = loginName;
    }

    public String getPwd() {
	return pwd;
    }

    public void setPwd(String pwd) {
	this.pwd = pwd;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public Integer getAge() {
	return age;
    }

    public void setAge(Integer age) {
	this.age = age;
    }

    public Integer getSex() {
	return sex;
    }

    public void setSex(Integer sex) {
	this.sex = sex;
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public String getCreateTime() {
	return createTime;
    }

    public void setCreateTime(String createTime) {
	this.createTime = createTime;
    }

    public String getMobile() {
	return mobile;
    }

    public void setMobile(String mobile) {
	this.mobile = mobile;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getQq() {
	return qq;
    }

    public void setQq(String qq) {
	this.qq = qq;
    }

    public String getAddress() {
	return address;
    }

    public void setAddress(String address) {
	this.address = address;
    }

}
