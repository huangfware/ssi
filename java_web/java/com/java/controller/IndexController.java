package com.java.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.java.service.IndexService;

/**
 * 首页类
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  
 * @version  [版本号, 2016年1月25日]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
@Controller
@RequestMapping("/")
public class IndexController {

    @Autowired
    public IndexService indexService;
    
    /**
     * 
     * 跳转到主页面
     * 	用户菜单，用户信息，首页显示信息的初始化
     * @return
     * @see [类、类#方法、类#成员]
     */
    @RequestMapping("/index")
    public ModelAndView toIndex() {
	
	return null;
    }
    
    
    @RequestMapping("/funnel")
    public String funnel() {
//	System.out.println("进入测试页面");
	return "funnel";
    }

}
