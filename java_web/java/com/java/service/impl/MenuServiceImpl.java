package com.java.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.bean.MenuInfo;
import com.java.dao.MenuDAO;
import com.java.service.MenuService;

@Service("menuService")
public class MenuServiceImpl implements MenuService {

	@Autowired
	private MenuDAO menuDAO;

	@Override
	public List<MenuInfo> selectMenusByUserId(String iid) {
	    // TODO Auto-generated method stub
	    return menuDAO.selectMenusByUserId(iid);
	}

	
	
	
}
