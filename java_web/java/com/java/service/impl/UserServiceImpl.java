package com.java.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.java.bean.UserInfo;
import com.java.dao.UserDAO;
import com.java.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	@Override
	public UserInfo selectUserByName(String username) {
	    return userDAO.selectUserByName(username);
	}
	
	

}
