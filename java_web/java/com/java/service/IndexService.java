package com.java.service;

import java.util.List;
/**
 * 
 * 首页信息，
 * 	用户信息
 * 	文章列表
 * 	天气信息
 * 	
 * @author  
 * @version  [版本号, 2016年1月25日]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public interface IndexService {

    /**
     * 
     * 用户信息，简介
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public List<String> selectDetails();
    
    /**
     * 文章列表
     * <功能详细描述>
     * @param currentPage
     * @param limit
     * @return
     * @see [类、类#方法、类#成员]
     */
    public List<Object> selectPages(int currentPage, int limit, int categoryId);
    
    /**
     * 天气信息
     * 		选中城市的天气详细信息
     * 		当天天气，7天预报，折线图，天气图标等
     * <功能详细描述>
     * @param city
     * @return
     * @see [类、类#方法、类#成员]
     */
    public List<Object> selectWeather(String city);
}
