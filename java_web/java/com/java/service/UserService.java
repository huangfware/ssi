package com.java.service;

import java.util.List;

import com.java.bean.UserInfo;
/**
 * 
 * <一句话功能简述>
 * <功能详细描述>
 * 
 * @author  
 * @version  [版本号, 2016年1月15日]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public interface UserService {
    /**
     * 
     * 根据用户名查询用户密码
     * <功能详细描述>
     * @param username
     * @return
     */
    public UserInfo selectUserByName(String username);
    

}
