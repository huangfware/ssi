package com.java.service;

import java.util.List;

import com.java.bean.MenuInfo;

public interface MenuService {

    public List<MenuInfo> selectMenusByUserId(String iid);

}
